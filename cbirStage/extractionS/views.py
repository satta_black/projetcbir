from django.shortcuts import render
from django.http import HttpResponse


def simple_view(request):
    return render(request, 'extractionSimple.html')
