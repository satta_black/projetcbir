
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('footer/', include('acceuil.urls')),
    path('', include('extractionS.urls'), name='simple'),
    path('', include('extractionC.urls'), name='combine'),
    path('', include('acceuil.urls'), name='home')
]
