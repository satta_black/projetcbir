from django.apps import AppConfig


class ExtractioncConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'extractionC'
