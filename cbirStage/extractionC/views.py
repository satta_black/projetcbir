from django.shortcuts import render
from django.http import HttpResponse


def combine_view(request):
    return render(request, 'extractionCombine.html')