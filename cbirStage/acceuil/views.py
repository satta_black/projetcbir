from django.shortcuts import render
from django.http import HttpResponse

def home_view(request):
    return render(request, 'accueil.html')


def footer_view(request):
    return render(request, 'footer.html')

def simple_view(request):
    return render(request, 'extractionSimple.html')

def combine_view(request):
    return render(request, 'extractionCombine.html')