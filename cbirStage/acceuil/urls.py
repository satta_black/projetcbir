from django.urls import path
from . import views

urlpatterns = [

    path("footer/", views.footer_view, name='footer'),
    path("", views.home_view, name='home'),


]